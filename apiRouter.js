let express = require('express');

exports.router = (function()
{
        var router = express.Router();

        /**
         * Home
         */
        router.get('/',function(req,res){
            res.setHeader('ContentType','text/plain');
            res.send('salut ca va ?');
        });

        return router;
})();