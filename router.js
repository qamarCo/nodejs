let express = require('express');

//Controllers
let userController =  require('./controllers/userController');
let homeController =  require('./controllers/homeController');


exports.router = (function()
{
        var router = express.Router();

        /**
         * Home
         */
        router.route('/').get(homeController.index);

        /**
         * Errors
         */
        router.use(function(req, res, next){
            res.status(404).render('errors/404.ejs');
        })


        return router;
})();