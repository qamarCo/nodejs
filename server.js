let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let router = require('./router').router;
let apiRouter = require('./apiRouter').router;


app
//BodyParser Configurations
.use(bodyParser.urlencoded({extended : true }))
.use(bodyParser.json()) 
//ApiRouter
.use('/api',apiRouter)
//ApiRouter
.use('/',router)
//Port
.listen(8081);